import ReactDOM from "react-dom";
import React from "react";
import Transcriber from "./components/transcriber";

const AppContainerEl = document.getElementById(`App`);

ReactDOM.render(
    (
        <div>
            <h2>Transcriber</h2>
            <Transcriber></Transcriber>
        </div>
    ),
    AppContainerEl
);
