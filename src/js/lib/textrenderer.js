import EventsObject from "./events";
import "fabric";
import _ from "underscore"
class TextRenderer extends EventsObject {
	constructor(opts) {
		super(opts);
		this.canvas = opts.canvas;
		this.fabric = new fabric.Canvas(this.canvas);
		this.playlist = opts.playlist;
		this.currentTime = 0;
		this._activeObjects = new Map();
		this._playlistArray = [];
	}

	setPlaylist(playlist) {
		this.playlist = playlist;
		this._playlistArray = arrayFromMap(this.playlist).sort((a, b) => {
			a.startTime > b.startTime ? 1 : -1;
		});
	} 

	seek(time) {
		this.currentTime = time;
		this.onTimeUpdate(time);
	}

	onTimeUpdate(time) {
		if (this._playlistArray.length === 0) return;
		const grouped = _.groupBy(this._playlistArray, item => time >= item.startTime && time < item.endTime);
		const toShow = grouped.true;
		const toRemove = grouped.false;
		if (toShow) {
			toShow.forEach(item => {
				if (!this._activeObjects.has(item.id)) {
					const obj = new fabric.IText(item.caption, {
						fill: `white`,
						fontFamily: `helvetica`,
						textBackgroundColor: 'rgba(0, 0, 0, 0.9)',
						top: 400,
						textAlign: "center",
						fontSize: 24
					});
					this.fabric.add(obj);
					obj.centerH();
					this._activeObjects.set(item.id, obj);
				}
			});
		}

		if (toRemove) {
			toRemove.forEach(item => {
				if (this._activeObjects.has(item.id)) {
					const obj = this._activeObjects.get(item.id);
					this._activeObjects.delete(item.id);
					this.fabric.remove(obj);
				}
			});
		}
	}
}

const arrayFromMap = map => Array.from(map.keys()).map(key => ({ id: key, ...map.get(key) }));

export default TextRenderer;