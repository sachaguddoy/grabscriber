class EventsObject {
    constructor() {
        this.events = new Map();
    }

    on(event, cb, ctx) {
        let arr = [];
        if (this.events.has(event)) {
            arr = this.events.get(event);
        }

        arr.push({ cb, ctx });
        this.events.set(event, arr);
    }

    off(event, cb) {
        if (this.events.has(event)) {
            const arr = this.events.get(event);
            const index = arr.findIndex(e => e.cb === cb);
            arr.splice(index, 1);
        }
    }

    trigger(event, ...args) {
        if (this.events.has(event)) {
            const arr = this.events.get(event);
            arr.forEach(({ cb, ctx }) => cb.call(ctx, ...args));
        }
    }
}

export default EventsObject;