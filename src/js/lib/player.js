import EventsObject from "./events";

class Player extends EventsObject {
    constructor(options) {
        super(options);
        this.canvasEl = options.canvas;
        this.videoEl = document.createElement(`video`);
        this.ctx = this.canvasEl.getContext(`2d`);
        this.src = options.src;
        this.render = this.render.bind(this);
        this.onTimeUpdate = this.onTimeUpdate.bind(this);
        this.onLoadedMetadata = this.onLoadedMetadata.bind(this);
        this.loadMedia(this.src);
    }

    loadMedia() {
        console.log(`Loading Video...`)
        this.setupEventListeners();

        loadUrl(this.src, this.videoEl)
            .then(() => {
                console.log(`Loaded Video!`)
                this.videoEl.addEventListener(`loadeddata`, () => {
                    


                    window.requestAnimationFrame(this.render);
                });
            });
    }

    setupEventListeners() {
        this.videoEl.addEventListener(`loadedmetadata`, this.onLoadedMetadata);
        this.videoEl.addEventListener(`timeupdate`, this.onTimeUpdate);


        window.addEventListener("gamepadconnected", function (e) {
            console.log("Gamepad connected at index %d: %s. %d buttons, %d axes.",
                e.gamepad.index, e.gamepad.id,
                e.gamepad.buttons.length, e.gamepad.axes.length);
            loop();
        });
        window.addEventListener("gamepaddisconnected", function () {
            gamepadInfo.innerHTML = "Waiting for gamepad.";
        });
        const _this = this;

        function loop() {
            var gp = navigator.getGamepads()[0];
            if (!gp) {
                console.log("no gamepad??")
            }

            if ((gp.buttons && gp.buttons.findIndex(b => b.pressed) > -1) || _this.playbuttonpressed) { 
            

                for (var b = 0, t = gp.buttons.length; b < t; b++) {
                    if (gp.buttons[17].pressed) {
                        if (_this.videoEl.paused) {
                            _this.play();
                            _this.playbuttonpressed = true;
                        }
                    } else if (!gp.buttons[17].pressed) {
                        if (!_this.videoEl.paused){
                            _this.pause();
                            _this.playbuttonpressed = false;
                            _this.seek((_this.videoEl.currentTime*1000) - 500)
                        }
                    }

                    if(gp.buttons[14].pressed) {
                        _this.seek((_this.videoEl.currentTime*1000) - 10)
                    }
                    if(gp.buttons[15].pressed) {
                        console.log(`seek`)
                        _this.seek((_this.videoEl.currentTime*1000) + 10)
                    }
                }
            }
            window.requestAnimationFrame(loop)
        };
    }

    render() {
        this.ctx.drawImage(this.videoEl, 0, 0, this.videoEl.width, this.videoEl.height);
        window.requestAnimationFrame(this.render);
    }

    play() {
        this.videoEl.play();
    }

    pause() {
        this.videoEl.pause();
    }

    seek(time) {
        this.videoEl.currentTime = time / 1000;
    }

    onTimeUpdate() {
        this.trigger(`timeupdate`, this.videoEl.currentTime * 1000);
    }

    onLoadedMetadata() {
        this.trigger(`loadedmetadata`, this.videoEl);
        this.ready = true

        this.canvasEl.height = this.videoEl.videoHeight;
        this.canvasEl.width = this.videoEl.videoWidth;

        this.videoEl.height = this.videoEl.videoHeight;
        this.videoEl.width = this.videoEl.videoWidth;
    }

    get duration() {
        return this.videoEl.duration * 1000;
    }
}

function loadUrl(origin, video) {
    return new Promise((resolve, rej) => {
        fetch(origin)
            .then(res => res.blob())
            .then(res => {
                video.removeAttribute(`src`);
                video.load();
                
                const url = URL.createObjectURL(res);
                video.src = url;
                
                resolve();
            })
    });
  }

export default Player;