import React from "react";
import CanvasPlayer from "../lib/player";
import SpritemapTimeline from "./spritemaptimeline";
import WaveSurfer from "wavesurfer";
import TextRenderer from "./textrenderer";

class Player extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			paused: true,
			ready: false,
			currentTime: 0,
			currentProgress: 0,
			duration: 0
		};

		this.canvasRef = React.createRef();
		this.onPlay = this.onPlay.bind(this);
		this.onPause = this.onPause.bind(this);
		this.onSeek = this.onSeek.bind(this);
	}

	componentDidMount() {
		this.player = new CanvasPlayer({
			canvas: this.canvasRef.current,
			src: this.props.src
		});

		this.player.on(`timeupdate`, this.onTimeUpdate, this);
		this.player.on(`loadedmetadata`, () => {
			console.log(`loaded`)
			this.setState({ duration: this.player.duration });
		});

		this.surfer = WaveSurfer.create({
			container: "#waveform-container",
		});

		this.surfer.load(this.props.src);

		this.surfer.toggleInteraction();

		window.addEventListener(`keydown`, e => {
			if (e.shiftKey && e.code === "Space") {
				this.togglePlay();
			}
		}, this)
	}

	onTimeUpdate(time) {
		if (isNaN(this.player.duration)) return;
		const percentage = time / this.player.duration * 100;
		this.setState({ currentTime: time, currentProgress: percentage });
		this.props.updatePlayerTime(time);
		this.surfer.seekTo(percentage / 100);
	}

	seek(time) {
		this.player.seek(time);
	}

	seekPercentage(time) {
		if (isNaN(this.player.duration)) return;
		this.player.seek(this.player.duration / 100 * parseFloat(time));
	}

	togglePlay() {
		if (this.player.videoEl.paused) {
			this.onPlay();
		} else {
			this.onPause();
		}
	}

	onPlay() {
		this.player.play();
	}

	onPause() {
		this.player.pause();
	}

	onSeek(e) {
		const { value } = e.target
		this.seekPercentage(parseInt(value));
	}

	render() {
		const { currentTime, duration, currentProgress } = this.state;
		return (
			<div className="player-wrapper" style={{ width: "854px", padding: "10px"}}>
				<div style={{ position: "relative" }}>
					<canvas className="player-output" ref={this.canvasRef} />
					<div
						className="text-renderer"
						style={{
							position: "absolute",
							width: "100%",
							height: "100%",
							top: "0",
							left: "0"
						}}
					>
						<TextRenderer playlist={this.props.playlist} currentTime={currentTime} />
					</div>
				</div>
				<SpritemapTimeline src={this.props.spritemap} duration={duration}/>
				<div id="waveform-container"></div>
				<div className="controls">
					<section className="controls-row">
						<button type="button" onClick={this.onPlay}>Play</button>
						<button type="button" onClick={this.onPause}>Pause</button>
					</section>
					<section className="controls-row">
						<input
							type="range"
							id="timeline"
							min={0}
							max={100}
							value={currentProgress}
							step={1}
							style={{ "width" : "100%" }}
							onChange={this.onSeek}
						/>
					</section>
				</div>
			</div>
		)
	}
}

export default Player;
