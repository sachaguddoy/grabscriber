import React from "react";
import Player from "./player";
import Transcriber from "./transcriber";

class Grabscriber extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			playerTime: 0,
			playlist: new Map()
		};

		this.updatePlayerTime = this.updatePlayerTime.bind(this);
		this.addItem = this.addItem.bind(this);
		this.removeItem = this.removeItem.bind(this);
		this.overridePlaylist = this.overridePlaylist.bind(this);
		this.clearPlaylist = this.clearPlaylist.bind(this);
	}

	render() {
		return <div style={{ display: `flex` }}>
			<div className="player">
				<Player
					updatePlayerTime={this.updatePlayerTime}
					src="https://dwo3ckksxlb0v.cloudfront.net/media/or/teTTXwfAQht/ma/a0QkQ7cgHbt/ByXmbI6xG7J_sbr_avMFoLc71H2.mp4"
					spritemap="https://dwo3ckksxlb0v.cloudfront.net/media/or/teTTXwfAQht/ma/a0QkQ7cgHbt/a0QkQ7cgHbt_thumbsprite.jpeg"
					playlist={this.state.playlist}
				/>
				<div className="transcriber">
					<Transcriber
						playerTime={this.state.playerTime}
						addItem={this.addItem}
						removeItem={this.removeItem}
						playlist={this.state.playlist}
						overridePlaylist={this.overridePlaylist}
						clearPlaylist={this.clearPlaylist}
					/>
				</div>
			</div>
		</div>
	}

	overridePlaylist(playlist) {
		this.setState({
			playlist
		})
	}

	addItem(item) {
		const map = new Map(this.state.playlist);

		this.setState({
			playlist: this.state.playlist.set(item.id, item)
		});
	}

	removeItem(id) {
		const map = new Map(this.state.playlist); // make a separate copy of the map
        map.delete(id);
        this.setState({ playlist: map });
	}

	updatePlayerTime(playerTime) {
		this.setState({ playerTime });
	}

	clearPlaylist() {
		const map = new Map()
		this.setState({ playlist:map });
	}

}

export default Grabscriber;
