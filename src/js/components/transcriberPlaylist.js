import React from "react";
import TranscriberPlaylistItem from "./transcriberPlaylistItem";

class TranscriberPlaylist extends React.Component {
    constructor(props) {
        super(props);

        this.renderTranscribePlaylistItems = this.renderTranscribePlaylistItems.bind(this);
    }

    render() {
        return <div>
                <div className="transcriber-playlist-wrapper" ref={el => { this.el = el; }}>
                    {
                        Array.from(this.props.playlist.keys()).map(key => this.renderTranscribePlaylistItems(this.props.playlist.get(key), key))
                    }
                </div>
            </div>
    }

    renderTranscribePlaylistItems(x, key) {
        return <TranscriberPlaylistItem key={key} item={x} removeItemFromPlaylist={this.props.removeItemFromPlaylist}></TranscriberPlaylistItem>
    }
}
export default TranscriberPlaylist;
