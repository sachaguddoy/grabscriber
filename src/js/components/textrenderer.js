import React from "react";
import TextRenderer from "../lib/textrenderer";

class TextPlayer extends React.Component {
	constructor(props) {
		super(props);
		this.canvasRef = React.createRef();
	}

	componentDidMount() {
		this.renderer = new TextRenderer({ canvas: this.canvasRef.current, playlist: this.props.playlist });
	}

	componentDidUpdate() {
		this.renderer.setPlaylist(this.props.playlist);
		console.log(this.props)
		this.renderer.seek(this.props.currentTime);
	}

	render() {
		return (
			<React.Fragment>
				<canvas
					width={854}
					height={480}
					ref={this.canvasRef}
					style={{
						"pointerEvents": "none"
					}}
				/>
			</React.Fragment>
		)
	}
}

export default TextPlayer;