import React from "react";

class TranscriberPlaylistItem extends React.Component {
    constructor(props) {
        super(props);
        console.log(this.props.caption);
        this.state = {
            caption: this.props.item.caption,
            editMode:false,
            editButtonText: "Edit"
        };

        this.removeItemFromPlaylist = this.removeItemFromPlaylist.bind(this);
        this.updateInput = this.updateInput.bind(this);
        this.toggleEditMode = this.toggleEditMode.bind(this);
    }

    render() {
        return <div className="transcriber-playlist-item-wrapper" style={{"backgroundColor":"white",padding:"10px", margin:"10px", "borderRadius":"3px", border:"2px solid #E8E8E8", "backgroundColor":"white"}}>
                    <div style={{padding:"10px", display:"flex", "flexDirection": "row", "justifyContent": "space-between", "borderBottom":"2px solid #E8E8E8"}}>
                        <div className="scriber-playlist-item">{this.props.item.srtStartTime} - {this.props.item.srtEndTime}</div>
                        <button
                            ref={(editButton) => { this.editButton = editButton; }}
                            className="transcriber-playlist-item"
                            onClick={this.toggleEditMode}>
                            {this.state.editButtonText}
                        </button>
                        <button className="transcriber-playlist-item" onClick={this.removeItemFromPlaylist}>Delete</button>
                    </div>


                    <textarea
                        ref={(captionArea) => { this.captionArea = captionArea; }}
                        disabled={!this.state.editMode}
                        rows="4" cols="50"
                        type="text"
                        value={this.state.caption}
                        onChange={this.updateInput}
                        style={{
                            transition: "none 0s ease 0s",
                            width: "100%",
                            border: "none",
                            "overflowY": "scroll",
                            padding:"10px"
                        }}
                        >

                        </textarea>
                </div>
    }

    toggleEditMode() {
        this.setState({"editMode": !this.state.editMode});
        if (this.state.editMode === true) {
            this.setState({"editButtonText": "Edit"})
        } else {
            this.setState({"editButtonText": "Save"})

        }
    }

    removeItemFromPlaylist() {
        this.props.removeItemFromPlaylist(this.props.item.id);
    }

    updateInput() {
        this.setState({caption : event.target.value})
    }
}

export default TranscriberPlaylistItem;
