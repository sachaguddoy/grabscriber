import React from "react";

class TranscriberInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            caption: "",
            currentStartTime: 0,
            hasStartTime: false
        };

        this.addItemToPlaylist = this.addItemToPlaylist.bind(this);
        this.updateInput = this.updateInput.bind(this);
        this.listenForSpeech = this.listenForSpeech.bind(this);
        this._handleKeyPress = this._handleKeyPress.bind(this);
        this.clearInput = this.clearInput.bind(this);
    }

    _handleKeyPress(e) {
        if (event.shiftKey) {
        }
        else if (e.key === 'Enter') {
            this.addItemToPlaylist();
            this.setState({hasStartTime: false});
        } else {
            if (this.state.hasStartTime === false) {
                console.log(this.props.playerTime);
                this.setState({hasStartTime: true});
                this.setState({currentStartTime: this.props.playerTime});
            }
        }
    }

    clearInput() {
        this.setState({
          caption: ""
        })
    }

    render() {
        return <div className="transcriber-input-wrapper" style={{"backgroundColor":"white", margin:"10px", "borderRadius":"3px", border:"2px solid #E8E8E8"}}>
                    <div style={{margin:"10px", padding:"10px", display:"flex", flexDirection: "row", "justifyContent": "space-between", "borderBottom":"2px solid #E8E8E8"}}>
                        <div>Caption</div>
                        <button onClick={this.clearInput}>Clear></button>
                        <button onClick={this.listenForSpeech}>Speak></button>
                    </div>

                    <textarea
                        rows="4" cols="50"
                        type="text"
                        value={this.state.caption}
                        onChange={this.updateInput}
                        onKeyUp={this._handleKeyPress}
                        style={{
                            transition: "none 0s ease 0s",
                            width: "100%",
                            border: "none",
                            "overflowY": "scroll",
                            padding:"0px 15px"

                        }}
                        ></textarea>


                </div>
    }

    listenForSpeech() {
        const SpeechRecognition = SpeechRecognition || webkitSpeechRecognition

        const recognition = new SpeechRecognition();
        recognition.start();
        console.log(`listening`)
        const _this = this;
        recognition.onresult = function(event) {
          console.log(event)
          var last = event.results.length - 1;
          var message = event.results[last][0].transcript;
          console.log('Confidence: ' + event.results[0][0].confidence);
          _this.setState({ caption: _this.state.caption + " " + message });
        }
    }

    updateInput(event) {
        this.setState({caption : event.target.value})
    }


    addItemToPlaylist(evt) {
        const startTime = Math.round(this.state.currentStartTime);
        const endTime = Math.round(this.props.playerTime);
        const srtStartTime = this.formatTimeForSrt(startTime);
        const srtEndTime = this.formatTimeForSrt(endTime);
        const caption = this.state.caption;
        this.props.addItemToPlaylist(startTime, endTime,srtStartTime, srtEndTime, caption);
        this.setState({
          caption: ""
        })
    }

    formatTimeForSrt(time){
        var timeLeft = time;
        var srtTimeString = ""
        var hours = Math.floor(time/3600000);
        timeLeft = time - hours*3600000;
        var minutes = Math.floor(timeLeft/60000);
        timeLeft = (timeLeft - minutes*60000);
        var seconds = Math.floor(timeLeft/1000);
        timeLeft = timeLeft - seconds*1000;
        var millis = Math.floor(timeLeft);
        srtTimeString = srtTimeString + this.forceDoubleDigitNumber(hours) + ":" + this.forceDoubleDigitNumber(minutes) + ":" + this.forceDoubleDigitNumber(seconds) + "," + this.forceTripleDigitMilli(millis);
        console.log(srtTimeString);
        return srtTimeString
    }

    forceDoubleDigitNumber(num){
        var formattedNumber = ("0" + num).slice(-2);
        return formattedNumber;
    }

    forceTripleDigitMilli(num){
        var formattedNumber = ('000' + num).substr(-3);
        return formattedNumber;
    }
}

export default TranscriberInput;
