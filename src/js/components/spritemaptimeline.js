import React from "react";

export const FRAME_INTERVAL_SECONDS = 1;
export const FRAMES_PER_LINE = 10;
export const FRAME_WIDTH = 160;
export const FRAME_HEIGHT = 90;
const VIDEO_WIDTH = 854;


class SpritemapTimeline extends React.Component {
    constructor(props) {
        super(props);
        this.canvasRef = React.createRef();
        this.mapRef = React.createRef();
        this.spriteRef = React.createRef();
        this.state = {
            showSprite: false,
            spriteBackgroundPos: "initial",
            spritePositionX: 0
        };
    }

    componentDidMount() {
        this.ctx = this.canvasRef.current.getContext(`2d`);
        this.renderTimeline(0, this.props.duration);
        const _this = this;

        this.canvasRef.current.addEventListener("mousemove", function(e) {
    
            const offsetX = e.clientX / _this.canvasRef.current.width;
            const timelineDuration = _this.props.duration / 1000;
            const targetTime = (timelineDuration * offsetX) + 0;
            
            const { x, y } = getFramePosition({
                frameTime: targetTime
            });
            
            _this.setState({ showSprite: true, spriteBackgroundPos: "-" + x + "px -" + y + "px", spritePositionX: e.clientX + 'px' })
		});
		
		this.canvasRef.current.addEventListener("mouseout", function(e) {
			_this.setState({ showSprite: false });
		});
    }

    render() {
        if (this.canvasRef.current) {
            this.renderTimeline(0, this.props.duration);
        }
        return (
            <div>
                <canvas width={VIDEO_WIDTH} height={90} ref={this.canvasRef} />
                <div style={{ position: "relative" }}>
                    <div>
                        <img
                            src={this.props.src}
                            ref={this.mapRef}
                            style={{"position": "absolute", "left": "-99999px" }}
                        />
                    </div>
                    <div
                        ref={this.spriteRef}
                        style={{
                            "display": this.state.showSprite ? "block" : "none",
                            "backgroundPosition": this.state.spriteBackgroundPos,
                            "left": this.state.spritePositionX,
                            "backgroundImage": `url('${this.props.src}')`,
                            width: "160px",
                            height: "90px",
                            position: "absolute",
                            transform: "translateX(-50%)",
                            top: "-180px"
                        }}>

                    </div>
                </div>
            </div>
        );
    }

    renderTimeline(from = 0, duration) {

        renderSpriteFrames({
            canvas: this.canvasRef.current,
            sprite: this.mapRef.current,
            from: 0,
            to: this.props.duration,
            height: FRAME_HEIGHT,
            width: this.canvasRef.current.width
        })

    }
}

export const renderSpriteFrames = ({ canvas, sprite, from, to, height, width, y: renderY, frameInterval = FRAME_INTERVAL_SECONDS, framesPerLine = FRAMES_PER_LINE, frameWidth = FRAME_WIDTH, frameHeight = FRAME_HEIGHT }) => {
	// if (!isFiniteNumber(from) || from < 0) {
	// 	throw new RangeError(`[from ${from}] needs to be a positive finite number.`);
	// }

	// if (!isFiniteNumber(to) || to < 0) {
	// 	throw new RangeError(`[to ${to}] needs to be a positive finite number.`);
	// }

	// if (to <= from) {
	// 	throw new RangeError(`[from: ${from}] needs to be strictly greater than [to: ${to}]`);
	// }

	const ctx = canvas.getContext(`2d`);

    const duration = (to - from) / 1000;

	// the frameHeight will always differ from the canvas we're trying to
	// render the frames into
	const scaleFactor = (height / frameHeight);
	const scaledToCanvasFrameWidth = frameWidth * scaleFactor;

	// We create an array that picks all the best matching frames times based
	// on the `from` and `to` provided arguments
    const expectedNbOfFramesToDraw = Math.ceil(width / scaledToCanvasFrameWidth);
	const targetFrameInterval = duration / ((expectedNbOfFramesToDraw) || 1);

	let actualCanvasFrameWidth = scaledToCanvasFrameWidth;
	let frameOffset = 0;
	let nbOfFramesToDraw = Math.ceil(duration / frameInterval);
	let canvasFrameInterval = frameInterval;
	if (targetFrameInterval > frameInterval) {
		nbOfFramesToDraw = expectedNbOfFramesToDraw;
		// if we have 500px, we have to draw 7 thumbnails, which have are "80px" wide
		// We calculate than 500/7 = 71, and calculate the offset to "center" the frames
		actualCanvasFrameWidth = width / nbOfFramesToDraw;
		frameOffset = (scaledToCanvasFrameWidth - actualCanvasFrameWidth) / 2;
		canvasFrameInterval = targetFrameInterval;
	}

	const framesTimes = Array(nbOfFramesToDraw).fill(0).map((val, i) => (
		from + (canvasFrameInterval * i)
	));

	const framesToDraw = framesTimes
		.map(time => getFramePosition({
			frameTime: time,
			frameInterval,
			framesPerLine,
			frameWidth,
			frameHeight
		}));

	// And we finally draw all of that in a canvas
	framesToDraw
		.forEach(({ x, y }, index) => {

			// quick math...
			const sourceX = x + (frameOffset / scaleFactor);

			// depending on whether we have gaps or not
			// we draw images differently
			let targetX;
			let repeatFrame = 1;
			if (targetFrameInterval < frameInterval) {

				const widthBeforeNextFrame = frameInterval / duration * width;
				targetX = widthBeforeNextFrame * index;

				repeatFrame = Math.ceil(widthBeforeNextFrame / actualCanvasFrameWidth);

			} else {
				targetX = actualCanvasFrameWidth * index;
			}

			for (let i = 0; i < repeatFrame; i++) {
				ctx.drawImage(
					sprite,
					sourceX,
					y,
					actualCanvasFrameWidth / scaleFactor,
					frameHeight,
					targetX + (i * actualCanvasFrameWidth),
					0,
					actualCanvasFrameWidth,
					height
                );
			}

		});

	return framesToDraw;
};

export const getFramePosition = ({ frameTime, frameInterval = FRAME_INTERVAL_SECONDS, framesPerLine = FRAMES_PER_LINE, frameWidth = FRAME_WIDTH, frameHeight = FRAME_HEIGHT }) => {

	// isPositiveInteger(`framesPerLine`, framesPerLine);
	// isPositiveInteger(`frameWidth`, frameWidth);
	// isPositiveInteger(`frameHeight`, frameHeight);

	// if (!isFiniteNumber(frameInterval) || frameInterval < 0) {
	// 	throw new RangeError(`[frameInterval ${frameInterval}] needs to be a positive finite number.`);
	// }

	// if (!isFiniteNumber(frameTime) || frameTime < 0) {
	// 	throw new RangeError(`[frameTime ${frameTime}] needs to be a positive finite number.`);
	// }

	// if (!isNumber(frameTime) || !Number.isFinite(frameTime) || frameTime < 0) {
	// 	throw new RangeError(`[frameTime: ${frameTime}] needs to be a positive finite number.`);
	// }

	// We round the time to the closest frame time available in the spritemap
	const closestMatchingTime = frameInterval * Math.round(frameTime / frameInterval);

	const number = closestMatchingTime / frameInterval;
	const line = Math.floor(number / framesPerLine);
	const column = number % framesPerLine;

	return {
		line,
		column,
		x: column * frameWidth,
		y: line * frameHeight
	};

};

function drawRect(ctx, x, y, width, height, color = "#FF0000") {
    ctx.rect(x, y, width, height);
    ctx.fillStyle=color;
    ctx.fill();
}

export default SpritemapTimeline;