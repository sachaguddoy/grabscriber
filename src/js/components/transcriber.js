import React from "react";
import fs from 'fs'
import parseSRT from 'parse-srt'
import TranscriberInput from "./transcriberInput";
import TranscriberPlaylist from "./transcriberPlaylist";

class Transcriber extends React.Component {
    constructor(props) {
        super(props);

        this.addItemToPlaylist = this.addItemToPlaylist.bind(this);
        this.removeItemFromPlaylist = this.removeItemFromPlaylist.bind(this);
        this.makeSrt = this.makeSrt.bind(this);
        this.parseSrt = this.parseSrt.bind(this);
        this.importSrtProgressEvent = this.importSrtProgressEvent.bind(this);
    }

    render() {
        return <div className="transcriber-wrapper">
                <TranscriberPlaylist playlist={this.props.playlist} removeItemFromPlaylist={this.removeItemFromPlaylist}></TranscriberPlaylist>
                <div style={{"borderBottom":"4px solid #E8E8E8", "marginTop":"10px"}}></div>
                <TranscriberInput addItemToPlaylist={this.addItemToPlaylist} playerTime={this.props.playerTime}></TranscriberInput>
                <button style={{ margin: "0 10px" }} className="transcriber-export-srt-button" onClick={this.makeSrt}>Download SRT File</button>
                <input type="file" onChange={ (e) => this.importSrt(e.target.files) } />
                <button style={{ margin: "0 10px" }} className="transcriber-export-srt-button" onClick={this.props.clearPlaylist}>Clear playlist</button>

            </div>
    }

    importSrtProgressEvent(progressEvent) {
        let newPlaylist = new Map();

        // By lines
        let lines = progressEvent.target.result.split('\n');
        let item = {};
        var currentLineType = "number";
        for(let line = 0; line < lines.length; line++) {
            console.log(lines[line]);
            console.log(currentLineType);
            console.log(!lines[line].trim() || 0 === lines[line].trim().length);

            if (currentLineType === "number") {
                item.id = this.makeId();
                currentLineType = "time";
            } else if (currentLineType === "time") {
                // Parse time
                let time = lines[line];
                let timeArray = time.split(' ');
                item.startTime = timeArray[0].trim();
                item.endTime = timeArray[2].trim();

                currentLineType = "caption";
            } else if (currentLineType === "caption") {
                if (!lines[line].trim() || 0 === lines[line].trim().length) {
                    item.caption
                    newPlaylist.set(item.id, item);
                    item = {};

                    currentLineType="number";
                } else {
                    if (item.caption) {
                        item.caption = item.caption + lines[line];
                    } else {
                        item.caption = lines[line];
                    }
                }
            }
        }
        this.props.overridePlaylist(newPlaylist);
    }

    importSrt(fileList) {
        var file;
        if (fileList.length > 0) {
            file = fileList[0];
            var newPlaylist = new Map();
            var reader = new FileReader();
            reader.onload = this.importSrtProgressEvent;
            reader.readAsText(file);
        }
    }

    addItemToPlaylist(startTime, endTime, srtStartTime, srtEndTime, caption) {
        const id = this.makeId();
        const item = {
            id,
            startTime,
            endTime,
            srtStartTime,
            srtEndTime,
            caption
        }

        this.props.addItem(item);
    }

    removeItemFromPlaylist(id) {
        this.props.removeItem(id);
    }

    makeId() {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for (var i = 0; i < 11; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

      return text;
    }

    makeSrt(){
        const arr = Array.from(this.props.playlist.keys());
        this.parseSrt(arr)
    }

    parseSrt(arr){
        var srtString = "";
        var counter = 1;
        for (let s of arr){
            var item = this.props.playlist.get(s);
            const start = item.startTime
            const end = item.endTime
            const caption = item.caption
            srtString = srtString + counter +"\n" + start + " --> " + end + "\n" + caption + "\n"
            counter++;
        }
        this.downloadStringAsFile("SRTFile.srt", srtString);
    }

    downloadStringAsFile(filename, text){
        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
        element.setAttribute('download', filename);

        element.style.display = 'none';
        document.body.appendChild(element);

        element.click();

        document.body.removeChild(element);
    }
}

export default Transcriber;
