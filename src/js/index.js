import ReactDOM from "react-dom";
import React from "react";
import Grabscriber from "./components/grabscriber";

const AppContainerEl = document.getElementById(`App`);

ReactDOM.render(
    (
        <div className="app">
            <Grabscriber></Grabscriber>
        </div>
    ),
    AppContainerEl
);
